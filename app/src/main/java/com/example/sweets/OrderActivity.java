package com.example.sweets;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.sweets.model.ModelOrder;
import com.example.sweets.model.ModelProduct;
import com.example.sweets.model.OrderNO;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Random;

public class OrderActivity extends AppCompatActivity implements View.OnClickListener {

    private static int orderId = 0;
    private ImageView imageView;
    private Dialog epicDialog;

    private TextView name, amount, txtDate, txtTime, price;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Button btnDatePicker, btnTimePicker, dialogButton, addOrder;
    private TextView count;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private ModelProduct product;
    private ActionBar actionBar;
    private ImageButton addBtn, minusBtn;
    private FirebaseFirestore db;
    private int quantity = 1;

    private TextView orderIdText;
    private TextView delivery, pPrice, priceAll;
    private View constraintLayout;

    private EditText addressEdit, apartmentEdit, phoneEdit;
    private int deliveryText = 500, priceAllText, priceText = 500;

    private String date, time, mAmount, mName, address, phoneNumber, apartmenNumber, dateTime;
    private Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        init();
        actionBar = getSupportActionBar();
        actionBar.setTitle("Оформить заказ");
        epicDialog = new Dialog(OrderActivity.this);
        date = txtDate.getText().toString();
        time = txtTime.getText().toString();


        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            product = arguments.getParcelable("Key");
            Picasso.get().load(product.getImageUrl()).into(imageView);
            name.setText(product.getName());


        }


        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity++;
                count.setText(quantity + "");
                price.setText("" + priceText * quantity + " c");
                pPrice.setText("" + priceText * quantity + " c");
                priceAllText = priceText * quantity + deliveryText;
                priceAll.setText("Итого: " + priceAllText);


            }
        });

        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantity > 1) {
                    quantity--;
                    count.setText(quantity + "");
                    price.setText("" + priceText * quantity + " c");
                    pPrice.setText("" + priceText * quantity + " c");
                    priceAllText = priceText * quantity + deliveryText;
                    priceAll.setText("Итого: " + priceAllText);

                }

            }
        });


    }

    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    AlertDialog.THEME_DEVICE_DEFAULT_DARK, new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    AlertDialog.THEME_DEVICE_DEFAULT_DARK, new TimePickerDialog.OnTimeSetListener() {

                @Override
                public void onTimeSet(TimePicker view, int hourOfDay,
                                      int minute) {

                    txtTime.setText(hourOfDay + ":" + minute);
                    time = hourOfDay + ":" + minute;
                }
            }, mHour, mMinute, false);
            timePickerDialog.show();
        }
        if (v == constraintLayout) {

            order(v);
          /* epicDialog.setContentView(R.layout.dialog);
            epicDialog.show();
*/
        }

    }

    private void init() {


        addressEdit = findViewById(R.id.editAddress);
        apartmentEdit = findViewById(R.id.Apartment_number);
        phoneEdit = findViewById(R.id.mobileNumber);


        btnDatePicker = findViewById(R.id.btn_date);
        btnTimePicker = findViewById(R.id.btn_time);

        constraintLayout = findViewById(R.id.progressBtn);
        constraintLayout.setOnClickListener(this);

        dialogButton = findViewById(R.id.dialog_button);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        imageView = findViewById(R.id.imageView);
        name = findViewById(R.id.name);
        //  amount = findViewById(R.id.amount);

        txtDate = findViewById(R.id.in_date);
        txtTime = findViewById(R.id.in_time);
        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);
        //addOrder.setOnClickListener(this);
        price = findViewById(R.id.price);

        addBtn = findViewById(R.id.plusBtn);
        minusBtn = findViewById(R.id.minusBtn);
        count = findViewById(R.id.count);

        delivery = findViewById(R.id.priceDelivery);
        pPrice = findViewById(R.id.priceProduct);

        priceAll = findViewById(R.id.priceAll);


    }

    public void order(View view) {

        orderId = random.nextInt(900) + 100;

        mAmount = count.getText().toString();
        mName = name.getText().toString();
        address = addressEdit.getText().toString();
        apartmenNumber = apartmentEdit.getText().toString();
        phoneNumber = phoneEdit.getText().toString();
        dateTime = date + " " + time;


        if (TextUtils.isEmpty(address)) {
            addressEdit.setError("Введите адрес");
            return;
        } else if (TextUtils.isEmpty(apartmenNumber)) {
            apartmentEdit.setError("Введите номер квартиры");
            return;
        } else if (TextUtils.isEmpty(phoneNumber)) {
            phoneEdit.setError("Введите номер телефона");
            return;
        }


        final ProgressButton progress = new ProgressButton(OrderActivity.this, view);
        progress.btnActivated();

        final DocumentReference documentReferenceNo = db.collection("admins").document(product.getAdminEmail())
                .collection("orderNo").document("No");

        documentReferenceNo.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                //Toast.makeText(OrderActivity.this,documentSnapshot.toObject(OrderNO.class).getNo()+"", Toast.LENGTH_LONG).show();
                OrderNO orderNO = documentSnapshot.toObject(OrderNO.class);

                int order = orderNO.getNo();
                order++;
                ModelOrder newOrder = new ModelOrder(priceAllText, product.getName(), dateTime, address, apartmenNumber, quantity + "", "" + orderId, product.getImageUrl(), currentUser.getEmail(),order);
                DocumentReference documentReference = db.collection("orders").document("" + orderId);
                DocumentReference documentReferenceAdmin = db.collection("admins").document(product.getAdminEmail()).collection("orders").document("" + orderId);
                DocumentReference documentReferenceUser = db.collection("Users").document(currentUser.getEmail()).collection("orders").document("" + orderId);


               orderNO.setNo(order++);

                documentReferenceNo.set(orderNO)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(OrderActivity.this, "dfdf", Toast.LENGTH_LONG).show();
                    }
                });


                documentReference.set(newOrder)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(OrderActivity.this, "dfdf", Toast.LENGTH_LONG).show();
                    }
                });

                documentReferenceUser.set(newOrder)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(OrderActivity.this, "dfdf", Toast.LENGTH_LONG).show();
                    }
                });

                documentReferenceAdmin.set(newOrder)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                progress.btnFinish();
                                //  Toast.makeText(OrderActivity.this, "dfdf", Toast.LENGTH_LONG).show();
                                epicDialog.setContentView(R.layout.dialog);
                                orderIdText = epicDialog.findViewById(R.id.orderID);
                                orderIdText.setText("ID заказа:" + orderId);
                                epicDialog.show();
                                dialogButton = epicDialog.findViewById(R.id.dialog_button);
                                dialogButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        epicDialog.dismiss();
                                        startActivity(new Intent(OrderActivity.this, MyOrdersActivity.class));
                                        finish();
                                    }
                                });

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(OrderActivity.this, "dfdf", Toast.LENGTH_LONG).show();
                    }
                });


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(OrderActivity.this, "Oldu", Toast.LENGTH_LONG).show();

            }
        });

         /* documentReferenceNo.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {


        });*/


    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
    }
}
