package com.example.sweets;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

class ProgressButton {
CardView cardView;
ConstraintLayout constraintLayout;
ProgressBar progressBar;
TextView textView;

    public ProgressButton(Context context, View v) {
        this.cardView =v.findViewById(R.id.cardView);
        this.constraintLayout = v.findViewById(R.id.cl);
        this.progressBar = v.findViewById(R.id.progressBar);
        this.textView = v.findViewById(R.id.textView);
    }

    void  btnActivated(){
        progressBar.setVisibility(View.VISIBLE);
textView.setVisibility(View.GONE);
    }
    void btnFinish(){
        textView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }
}
