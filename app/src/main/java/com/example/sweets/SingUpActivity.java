package com.example.sweets;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.system.StructTimespec;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class SingUpActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText fullname;
    private EditText Email;
    private EditText phone;
    private EditText Password;
    private View singUpBtn;
    private FirebaseUser currentUser;
    private FirebaseFirestore db;
    private LinearLayout loginA;
    private String userID;


    private static final String TAG = "MyActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);
        fullname = findViewById(R.id.fullName);
        Email = findViewById(R.id.userEmailId);
        phone = findViewById(R.id.mobileNumber);
        Password = findViewById(R.id.password);
        singUpBtn = findViewById(R.id.signUpBtn);
        loginA=findViewById(R.id.forLogIn);


        loginA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SingUpActivity.this,LoginActivity.class));
                finish();
            }
        });




        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
     //   currentUser = mAuth.getCurrentUser();


        singUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String email = Email.getText().toString();
                final String password = Password.getText().toString();
                 final   String ph = phone.getText().toString();
                final String fn = fullname.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Email.setError("Введите почту");
                    return;
                }
              else   if (TextUtils.isEmpty(password)) {
                    Password.setError("Введите пароль");
                    return;
                }
                else if (password.length() < 6) {
                    Password.setError("Введите минимум 6 символ");
                    return;
                }


                final ProgressButton progress = new ProgressButton(SingUpActivity.this, singUpBtn);
                progress.btnActivated();

                mAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    // Sign in success, update UI with the signed-in user's information
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    userID = user.getUid();

                                    DocumentReference documentReference = db.collection("Users").document(user.getEmail());

                                    Map<String, Object> us = new HashMap<>();
                                    us.put("name", fn);
                                    us.put("email", email);
                                    us.put("phone",ph);
                                    us.put("password", password);

                                    documentReference.set(us).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {

                                            progress.btnFinish();
                                            Intent intent = new Intent(SingUpActivity.this, OrderActivity.class);
                                            startActivity(intent);
                                            finish();



                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                        }
                                    });

                                } else {
                                    // If sign in fails, display a message to the user.
                                    Toast.makeText(SingUpActivity.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });


            }
        });

    }


}
