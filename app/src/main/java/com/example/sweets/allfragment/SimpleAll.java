package com.example.sweets.allfragment;

public class SimpleAll {
    private String name;
    private int imageResourceId;
    private int price;

    public SimpleAll(String name, int imageResourceId, int price) {
        this.name = name;
        this.imageResourceId = imageResourceId;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public int getPrice() {
        return price;
    }
}
