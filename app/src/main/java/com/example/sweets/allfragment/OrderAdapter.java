package com.example.sweets.allfragment;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sweets.DetailActivity;
import com.example.sweets.R;
import com.example.sweets.model.ModelOrder;
import com.example.sweets.model.ModelProduct;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.squareup.picasso.Picasso;

public class OrderAdapter extends FirestoreRecyclerAdapter<ModelOrder,OrderAdapter.ViewHolder> {


    public OrderAdapter(@NonNull FirestoreRecyclerOptions<ModelOrder> options) {
        super(options);
    }


    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull ModelOrder model) {

        holder.bind(model);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_myorders,parent,false);
        return new ViewHolder(view);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,price,date,orderId,quantity,address;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            address=itemView.findViewById(R.id.address);
            date=itemView.findViewById(R.id.date);
            orderId=itemView.findViewById(R.id.orderID);
            name=itemView.findViewById(R.id.name);
            price=itemView.findViewById(R.id.price);
            quantity=itemView.findViewById(R.id.quantity);

        }

        public void bind(ModelOrder model) {

            address.setText("Адрес доставки :"+model.getAddress());
            name.setText("Имя :"+model.getProductName());
            quantity.setText("Количество :"+model.getQuantity());
            orderId.setText("ID заказа:"+model.getOrderID());
            price.setText("Сумма заказа:"+model.getAllPrice());
            date.setText("Время:"+model.getDateOrder());

        }
    }

}
