package com.example.sweets.allfragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sweets.R;
import com.example.sweets.model.ModelProduct;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Random;


public class AllFragment extends Fragment {
    private FirebaseFirestore db;
    private ProductAdapter adapter;
    private RecyclerView recyclerView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        db=FirebaseFirestore.getInstance();
        View mMainView = inflater.inflate(R.layout.fragment_all, container,
                false);
        com.google.firebase.firestore.Query  query=db.collection("products");
        FirestoreRecyclerOptions<ModelProduct> options=new FirestoreRecyclerOptions.Builder<ModelProduct>()
                .setQuery(query,ModelProduct.class)
                .build();

        adapter =new ProductAdapter(options);
        recyclerView=mMainView.findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        return recyclerView;

    }

    @Override
    public void onStop() {
        super.onStop();
    adapter.stopListening();
    }

    @Override
    public void onStart() {
        super.onStart();
    adapter.startListening();
    }
}
