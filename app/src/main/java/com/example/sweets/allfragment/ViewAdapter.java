package com.example.sweets.allfragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sweets.DetailActivity;
import com.example.sweets.MainActivity;
import com.example.sweets.R;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.View_Holder> {
    private String[] captions;
    private int[] imageIds;

    public ViewAdapter(String[] captions, int[] imageIds) {
        this.captions = captions;
        this.imageIds = imageIds;
    }


    @NonNull
    @Override
    public View_Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.items, parent, false);
        View_Holder holder = new View_Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull View_Holder holder, int position) {

        holder.bind(imageIds[position]);
        CardView cardView = holder.cardView;
        ImageView imageView = (ImageView) cardView.findViewById(R.id.info_image);
        TextView textView = cardView.findViewById(R.id.info_text);
        Drawable drawable = ContextCompat.getDrawable(cardView.getContext(), imageIds[position]);
        imageView.setImageDrawable(drawable);
        imageView.setContentDescription(captions[position]);
        textView.setText(captions[position]);

    }

    @Override
    public int getItemCount() {
        return captions.length;
    }

    public static class View_Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cardView;
        int imageId;
        public View_Holder(@NonNull View itemView) {
            super(itemView);
            cardView = (CardView) itemView;
            cardView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            Intent intent =new Intent(v.getContext(), DetailActivity.class);
            intent.putExtra("ImageId",imageId);
            v.getContext().startActivity(intent);
          }
        void  bind(int imageId){
            this.imageId=imageId;
        }
    }
}
