package com.example.sweets.allfragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.sweets.DetailActivity;
import com.example.sweets.R;
import com.example.sweets.model.ModelOrder;
import com.example.sweets.model.ModelProduct;
import com.example.sweets.model.ProductParceble;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.squareup.picasso.Picasso;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

public class ProductAdapter extends FirestoreRecyclerAdapter<ModelProduct,ProductAdapter.ViewHolder>{


     public ProductAdapter(@NonNull FirestoreRecyclerOptions<ModelProduct> options) {
        super(options);

    }


    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull ModelProduct model) {


        holder.bind(model);
       // parceble=new ProductParceble(model.getName(),model.getCategori(),model.getPrice(),model.getDescriptions(),model.getWeight(),model.getImageUrl());

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.items,parent,false);
        return new ViewHolder(view);

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name,price;
        ImageView imageView;
        ModelProduct modelProduct;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        name=itemView.findViewById(R.id.info_text);
        price=itemView.findViewById(R.id.price);
        imageView=itemView.findViewById(R.id.info_image);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent =new Intent(v.getContext(), DetailActivity.class);
        intent.putExtra("Key",modelProduct);
      //  intent.putExtra("Email",modelProduct.getAdminEmail());
        v.getContext().startActivity(intent);

    }

        public void bind(ModelProduct model) {
            modelProduct=model;
            Picasso.get().load(model.getImageUrl()).into(imageView);
            price.setText(" $ " +model.getPrice());
            name.setText(model.getName());

    }
    }
}
