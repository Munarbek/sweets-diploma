package com.example.sweets;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sweets.model.ModelProduct;
import com.example.sweets.model.ProductParceble;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    ImageView imageView;
    TextView mName, mHetto, mDesctipt,infoAdmin,mPrice;
    ActionBar actionBar;
    Menu menu;
    Button button;
    private int drawableResourceId = R.drawable.ic_favorite_click;
    private int drawableResourceId2 = R.drawable.ic_favorite;
    private int count = 2;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference documentReference;
    private ModelProduct product;
    private String userEmail;
    private    FirebaseUser currentUser;
    private  boolean favorite=false;

    @Override
    protected void onStart() {
        super.onStart();
        currentUser = mAuth.getCurrentUser();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        db = FirebaseFirestore.getInstance();
        actionBar = getSupportActionBar();
        mAuth = FirebaseAuth.getInstance();
        currentUser=mAuth.getCurrentUser();
        actionBar.setTitle(" ");
        actionBar.setDisplayHomeAsUpEnabled(true);

        imageView = findViewById(R.id.imageD);
        mName = findViewById(R.id.name);
        mDesctipt = findViewById(R.id.descript);
        mHetto = findViewById(R.id.hetto);
        infoAdmin=findViewById(R.id.infoAdmin);
        mPrice=findViewById(R.id.price);
        Bundle arguments = getIntent().getExtras();


        if (arguments != null) {
            product = arguments.getParcelable("Key");
            mName.setText(product.getName());
            mDesctipt.setText("Описания:"+"\n"+product.getDescriptions());
            mHetto.setText("Масса: "+product.getWeight()+"г");
            Picasso.get().load(product.getImageUrl()).into(imageView);
            mPrice.setText(product.getPrice());
        }

        getFavorite();






        button = findViewById(R.id.to_Order);

        //  actionBar.setDisplayShowHomeEnabled(true);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentUser = mAuth.getCurrentUser();
                if (currentUser != null) {
                    Intent intent = new Intent(DetailActivity.this, OrderActivity.class);
                    intent.putExtra("Key",product);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(DetailActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });








    }

    private void getFavorite() {
        if (currentUser != null) {

            documentReference = db.collection("Users").document(currentUser.getEmail()).collection("fovorites").document(product.getName());
            documentReference.get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {

                                favorite = true;
                                count++;
                                menu.findItem(R.id.favorite).setIcon(drawableResourceId2);

                            } else {
                                menu.findItem(R.id.favorite).setIcon(drawableResourceId);
                                favorite = false;

                                //  Toast.makeText(DetailActivity.this,"name" +"Olduu" ,Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(DetailActivity.this, "name" + e, Toast.LENGTH_LONG).show();

                        }
                    });

        }
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        MenuInflater inflater = getMenuInflater();
        this.menu = menu;
        inflater.inflate(R.menu.appbar_menu, menu);
           /* if (favorite==true)
            menu.findItem(R.id.favorite).setIcon(drawableResourceId2);
            else
            menu.findItem(R.id.favorite).setIcon(drawableResourceId);*/

        //menu.findItem(R.id.favorite).setIcon(drawableResourceId);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            count++;
            documentReference=db.collection("Users").document(currentUser.getEmail()).collection("fovorites").document(product.getName());
            switch (item.getItemId()) {
                case R.id.favorite:
                Drawable icon=item.getIcon();

                    if (count%2==0){
                        item.setIcon(R.drawable.ic_favorite_click);
                        documentReference.delete();
                    }

                    else
                    {     item.setIcon(R.drawable.ic_favorite);
                           documentReference.set(product)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
                    }
                    break;
            }
        } else {

            Toast.makeText(DetailActivity.this, "Сначала нужно войти", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(DetailActivity.this, LoginActivity.class);
            startActivity(intent);

        }


        return super.onOptionsItemSelected(item);
    }


}