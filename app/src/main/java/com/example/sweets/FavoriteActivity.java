package com.example.sweets;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.sweets.allfragment.ProductAdapter;
import com.example.sweets.model.ModelProduct;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class FavoriteActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private ProductAdapter adapter;
    private RecyclerView recyclerView;
    private FirebaseAuth mAuth;
    private Toolbar toolbar;
    private Button loginD;
private LinearLayout layout;
private  Button logInBtn;
private  FirebaseUser currentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        db=FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        recyclerView=findViewById(R.id.rvFavorite);

        layout=findViewById(R.id.snachala);
        logInBtn=findViewById(R.id.favoriteLoginBtn);
        logInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FavoriteActivity.this,LoginActivity.class));
            }
        });



        toolbar = findViewById(R.id.toolbar_favorite);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
                finish();
            }
        });



    }


    @Override
    public void onStop() {
        super.onStop();
        if (currentUser!=null)
        adapter.stopListening();

    }

    @Override
    public void onStart() {
        super.onStart();

            if (currentUser!=null){
                layout.setVisibility(View.INVISIBLE);
                com.google.firebase.firestore.Query  query=db.collection("Users").document(currentUser.getEmail()).collection("fovorites");
                FirestoreRecyclerOptions<ModelProduct> options=new FirestoreRecyclerOptions.Builder<ModelProduct>()
                        .setQuery(query,ModelProduct.class)
                        .build();
                adapter =new ProductAdapter(options);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
                adapter.startListening();}
            else {

                recyclerView.setVisibility(View.GONE);

            }


        }











}
