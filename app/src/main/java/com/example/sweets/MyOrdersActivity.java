package com.example.sweets;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.sweets.allfragment.OrderAdapter;
import com.example.sweets.allfragment.ProductAdapter;
import com.example.sweets.model.ModelOrder;
import com.example.sweets.model.ModelProduct;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class MyOrdersActivity extends AppCompatActivity {

    TextView textView;
    private FirebaseFirestore db;
    private OrderAdapter adapter;
    private RecyclerView recyclerView;
    private FirebaseAuth auth;
    private FirebaseUser currentUser;
private DocumentReference documentReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);


        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();

      /*  documentReference=db.collection("Users").document(currentUser.getEmail()).collection("orders").document("220");

        documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                ModelOrder city = documentSnapshot.toObject(ModelOrder.class);

                textView.setText(city.getOrderID());

            }
        });*/



        recyclerView=findViewById(R.id.orderRv);

        com.google.firebase.firestore.Query query = db.collection("Users").document(currentUser.getEmail()).collection("orders")
                .orderBy("orderNo", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<ModelOrder> options = new FirestoreRecyclerOptions.Builder<ModelOrder>()
                .setQuery(query, ModelOrder.class)
                .build();

        adapter = new OrderAdapter(options);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }


    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }
}
