package com.example.sweets;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private LinearLayout createAccount;
    private ActionBar actionBar;
    private FirebaseAuth mAuth;
    private EditText eEmail, ePassword;
    private String email, password;
    private View logIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        mAuth = FirebaseAuth.getInstance();

        eEmail = findViewById(R.id.login_email);
        ePassword = findViewById(R.id.login_password);
        logIn=findViewById(R.id.loginBtn);


        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email=eEmail.getText().toString();
                password=ePassword.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    eEmail.setError("Введите почту");
                    return;
                }
               else if (TextUtils.isEmpty(password)) {
                    ePassword.setError("Введите пароль");
                    return;
                }
                if (password.length() < 6) {
                    ePassword.setError("Введите минимум 6 символ");
                    return;
                }
                final ProgressButton progress = new ProgressButton(LoginActivity.this,logIn);
                progress.btnActivated();

                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    FirebaseUser user = mAuth.getCurrentUser();
                                    progress.btnFinish();
                                    finish();

                                } else {

                                    Toast.makeText(LoginActivity.this, "Не верно email или пароль.",
                                            Toast.LENGTH_SHORT).show();
                                }

                            }
                        });



            }
        });


        createAccount = findViewById(R.id.createAccount);
        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SingUpActivity.class);
                startActivity(intent);
                finish();

            }
        });


    }

    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser!=null){
            finish();
        }

    }
}
