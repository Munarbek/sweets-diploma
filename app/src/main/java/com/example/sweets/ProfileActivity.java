package com.example.sweets;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sweets.model.ModelUder;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


public class ProfileActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private DocumentReference documentReference;
        private TextView fullName,userEmail,mobleNumber;
    private FirebaseAuth mAuth;
            private FirebaseUser currentUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        db=FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        fullName=findViewById(R.id.fullName);
        userEmail=findViewById(R.id.userEmailId);
        mobleNumber=findViewById(R.id.mobileNumber);
        currentUser = mAuth.getCurrentUser();


        documentReference=db.collection("Users").document(currentUser.getEmail());

        documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                ModelUder city = documentSnapshot.toObject(ModelUder.class);
                fullName.setText(city.getName());
                userEmail.setText(city.getEmail());
                mobleNumber.setText(city.getPhone());


            }
        });

    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.


    }
}
