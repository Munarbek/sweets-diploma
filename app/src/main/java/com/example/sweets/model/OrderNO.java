package com.example.sweets.model;

public class OrderNO {
    private  int No;

    public OrderNO() {

    }

    public OrderNO(int no) {
        No = no;
    }

    public int getNo() {
        return No;
    }

    public void setNo(int no) {
        No = no;
    }
}
