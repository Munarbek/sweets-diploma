package com.example.sweets.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelProduct implements  Parcelable {

        private  String adminEmail;
        private String name;
        private String categori;
        private String price;
        private String descriptions;
        private String weight;
        private String imageUrl;



    public static final Creator<ModelProduct> CREATOR = new Creator<ModelProduct>() {
        @Override
        public ModelProduct createFromParcel(Parcel in) {
            return new ModelProduct(in);
        }

        @Override
        public ModelProduct[] newArray(int size) {
            return new ModelProduct[size];
        }
    };



    public  ModelProduct(){}

    public ModelProduct(String adminEmail, String name, String categori, String price, String descriptions, String weight, String imageUrl) {
        this.adminEmail = adminEmail;
        this.name = name;
        this.categori = categori;
        this.price = price;
        this.descriptions = descriptions;
        this.weight = weight;
        this.imageUrl = imageUrl;
    }

    protected ModelProduct(Parcel in) {
        adminEmail=in.readString();
        name = in.readString();
        categori = in.readString();
        price = in.readString();
        descriptions = in.readString();
        weight = in.readString();
        imageUrl = in.readString();
    }



    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }
    public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategori() {
            return categori;
        }

        public void setCategori(String categori) {
            this.categori = categori;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDescriptions() {
            return descriptions;
        }

        public void setDescriptions(String descriptions) {
            this.descriptions = descriptions;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(adminEmail);
        dest.writeString(name);
        dest.writeString(categori);
        dest.writeString(price);
        dest.writeString(descriptions);
        dest.writeString(weight);
        dest.writeString(imageUrl);
    }
}


