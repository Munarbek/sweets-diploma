package com.example.sweets.model;


import android.os.Parcel;
import android.os.Parcelable;

public class ModelOrder implements Parcelable {



    private int allPrice;
    private String productName;

    private String dateOrder;
    private String address;

    private String nomerKvartiry;
    private String quantity;

    private String orderID;
    private String urlImage;

    private String emailUser;
    private  int orderNo;

    public ModelOrder(int allPrice, String productName, String dateOrder, String address, String nomerKvartiry, String quantity, String orderID, String urlImage, String emailUser, int orderNo) {
        this.allPrice = allPrice;
        this.productName = productName;
        this.dateOrder = dateOrder;
        this.address = address;
        this.nomerKvartiry = nomerKvartiry;
        this.quantity = quantity;
        this.orderID = orderID;
        this.urlImage = urlImage;
        this.emailUser = emailUser;
        this.orderNo = orderNo;
    }

    protected ModelOrder(Parcel in) {
        allPrice = in.readInt();
        productName = in.readString();
        dateOrder = in.readString();
        address = in.readString();
        nomerKvartiry = in.readString();
        quantity = in.readString();
        orderID = in.readString();
        urlImage = in.readString();
        emailUser = in.readString();
        orderNo=in.readInt();
    }

    public static final Creator<ModelOrder> CREATOR = new Creator<ModelOrder>() {
        @Override
        public ModelOrder createFromParcel(Parcel in) {
            return new ModelOrder(in);
        }

        @Override
        public ModelOrder[] newArray(int size) {
            return new ModelOrder[size];
        }
    };

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public ModelOrder() {


    }

    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public int getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(int allPrice) {
        this.allPrice = allPrice;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNomerKvartiry() {
        return nomerKvartiry;
    }

    public void setNomerKvartiry(String nomerKvartiry) {
        this.nomerKvartiry = nomerKvartiry;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(allPrice);
        dest.writeString(productName);
        dest.writeString(dateOrder);
        dest.writeString(address);
        dest.writeString(nomerKvartiry);
        dest.writeString(quantity);
        dest.writeString(orderID);
        dest.writeString(urlImage);
        dest.writeString(emailUser);
        dest.writeInt(orderNo);
    }
}
